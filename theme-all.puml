
'==================================================================================================
' COLORS
'==================================================================================================

' Predefined and default color definitions.

'------------------------------------------------
' Predefined colors

' Each color comes in a set as a LIGHT and DARK style.
' Naming convention: {LIGHT,DARK}COLOR

'------------------------------------------------
' BLUE

!ifdef DARKBLUE
  !define DARKSTYLE
  !$LIGHTCOLOR ?= "1a66c2"
  !$DARKCOLOR ?= "002642"
!endif

!ifdef LIGHTBLUE
  !define LIGHTSTYLE
  !$LIGHTCOLOR ?= "2a86e2"
  !$DARKCOLOR ?= "1a66c2"
!endif

'------------------------------------------------
' GREEN

!ifdef DARKGREEN
  !define DARKSTYLE
  !$LIGHTCOLOR ?= "228811"
  !$DARKCOLOR ?= "113300"
!endif

!ifdef LIGHTGREEN
  !define LIGHTSTYLE
  !$LIGHTCOLOR ?= "55BB33"
  !$DARKCOLOR ?= "338822"
!endif

'------------------------------------------------
' ORANGE

!ifdef DARKORANGE
  !define DARKSTYLE
  !$LIGHTCOLOR ?= "BB6600"
  !$DARKCOLOR ?= "662200"
!endif

!ifdef LIGHTORANGE
  !define LIGHTSTYLE
  !$LIGHTCOLOR ?= "FF8800"
  !$DARKCOLOR ?= "BB6600"
!endif

'------------------------------------------------
' RED

!ifdef DARKRED
  !define DARKSTYLE
  !$LIGHTCOLOR ?= "880000"
  !$DARKCOLOR ?= "330000"
!endif

!ifdef LIGHTRED
  !define LIGHTSTYLE
  !$LIGHTCOLOR ?= "CC0033"
  !$DARKCOLOR ?= "AA0033"
!endif


'------------------------------------------------
' Color Fallback (LIGHTBLUE)

!ifdef !LIGHTSTYLE && !DARKSTYLE
  !define LIGHTSTYLE
  !$LIGHTCOLOR ?= "2a86e2"
  !$DARKCOLOR ?= "1a66c2"
!endif


'------------------------------------------------
' Transparent Background

!ifdef TRANSPARENT
  !ifdef DARKSTYLE
    !$BORDERCOLOR ?= "aaa"
  !endif
  !$SHADOW ?= "false"
  !$ACCENT ?= "transparent"
  !$BG ?= "transparent"
  !$BOXBG ?= "transparent"
  !$DARKBG ?= "transparent"
  !$LIGHTBG ?= "transparent"
!endif


'------------------------------------------------
' Style definitions

!ifdef LIGHTSTYLE
  !$BG ?= "fff"
  !$PRIMARYFONTCOLOR ?= "000"
  !$SECONDARYFONTCOLOR ?= "333"
  !$ARROWCOLOR ?= "000"
  !$ARROWFONTCOLOR ?= "333"
  !$BORDERCOLOR ?= "aaa"
  !$BOXBG ?= "ccc"
  !$LIFELINEBG ?= "aaa"
  !$DARKBG ?= $DARKCOLOR
  !$LIGHTBG ?= $LIGHTCOLOR
!endif

!ifdef DARKSTYLE
  !$BG ?= "777"
  !$PRIMARYFONTCOLOR ?= "fff"
  !$SECONDARYFONTCOLOR ?= "aaa"
  !$ARROWCOLOR ?= "fff"
  !$ARROWFONTCOLOR ?= "bbb"
  !$BORDERCOLOR ?= "1b1b1b"
  !$BOXBG ?= "2e2e2e"
  !$LIFELINEBG ?= "1b1b1b"
  !$DARKBG ?= $DARKCOLOR
  !$LIGHTBG ?= $LIGHTCOLOR
!endif



'==================================================================================================
' DEFINITIONS
'==================================================================================================

' Define variables and precedures

!$FONTNAME ?= "Verdana"
!$FONTSIZE ?= 11
!$CIRCLERADIUS ?= 9
!$SHADOW ?= "true"


'------------------------------------------------
' Procedures

!procedure font_style()
  fontColor $PRIMARYFONTCOLOR
  fontName $FONTNAME
  fontSize $FONTSIZE
  stereotypeFontColor $SECONDARYFONTCOLOR
  stereotypeFontSize $FONTSIZE
  attributeFontColor $SECONDARYFONTCOLOR
  attributeFontSize $FONTSIZE
!endprocedure

!procedure basic_style()
  backgroundColor $BOXBG
  borderColor $BORDERCOLOR
  stereotypeABackgroundColor $LIGHTBG
  stereotypeCBackgroundColor $LIGHTBG
  stereotypeEBackgroundColor $LIGHTBG
  stereotypeIBackgroundColor $LIGHTBG
  stereotypeNBackgroundColor $LIGHTBG
!endprocedure

!procedure light_style()
  backgroundColor $LIGHTBG
  borderColor $LIGHTBORDERCOLOR
!endprocedure

!procedure LIGHTCOLOR_style()
  backgroundColor $LIGHTBG
  !ifdef TRANSPARENT
    borderColor $LIGHTCOLOR
  !else
    borderColor $DARKCOLOR
  !endif
!endprocedure

!procedure arrow_style()
  arrowColor $ARROWCOLOR
  arrowFontName $FONTNAME
  arrowFontColor $ARROWFONTCOLOR
  arrowFontSize $FONTSIZE
!endprocedure



'==================================================================================================
' APPLIMENT
'==================================================================================================

' Apply skin parameters

'------------------------------------------------
' Global defaults

skinparam {
  basic_style()
  font_style()
  arrow_style()
  defaultFontColor $PRIMARYFONTCOLOR
  defaultFontName $FONTNAME
  defaultFontSize $FONTSIZE
  backgroundColor $BG
  shadowing $SHADOW
}

'------------------------------------------------
' Class diagrams

skinparam {
  circledCharacter {
    radius $CIRCLERADIUS
    fontSize $FONTSIZE
    fontName $FONTNAME
  }

  class {
    basic_style()
    font_style()
    arrow_style()
    attributeIconSize $FONTSIZE
  }

  interface {
    LIGHTCOLOR_style()
    font_style()
  }
}

'------------------------------------------------
' Sequence diagrams

skinparam {
  sequence {
    font_style()
    arrow_style()
    lifeLineBorderColor $LIGHTCOLOR
    lifeLineBackgroundColor $LIFELINEBG
  }

  actor {
    LIGHTCOLOR_style()
    font_style()
  }

  participant {
    basic_style()
    font_style()
  }

  collections {
    basic_style()
    font_style()
  }

  sequenceBox {
    light_style()
    font_style()
  }

  boundary {
    LIGHTCOLOR_style()
    font_style()
  }

  control {
    LIGHTCOLOR_style()
    font_style()
  }

  entity {
    LIGHTCOLOR_style()
    font_style()
  }
}

'------------------------------------------------
' Component diagrams

skinparam {
  component {
    basic_style()
    font_style()
  }

  package {
    basic_style()
    font_style()
  }

  node {
    basic_style()
    font_style()
  }

  database {
    basic_style()
    font_style()
  }

  queue {
    basic_style()
    font_style()
  }
}

'------------------------------------------------
' Use Case diagrams

skinparam {
  usecase {
    basic_style()
    font_style()
    arrow_style()
  }
}

'------------------------------------------------
' Use Case diagrams

skinparam {
  activity {
    basic_style()
    font_style()
    arrow_style()
    startColor $LIGHTCOLOR
    endColor $LIGHTCOLOR
    borderColor $DARKCOLOR
    barColor $LIGHTCOLOR
  }

  activityDiamond {
    basic_style()
    font_style()
    arrow_style()
  }

  person {
    basic_style()
    font_style()
  }
}

'------------------------------------------------
' State diagrams

skinparam state {
  basic_style()
  font_style()
  arrow_style()
  startColor $LIGHTCOLOR
  endColor $LIGHTCOLOR
}

'------------------------------------------------
' Object diagrams

skinparam object {
  basic_style()
  font_style()
  arrow_style()
}

'------------------------------------------------
' Deployment diagrams

skinparam {
  frame {
    basic_style()
    font_style()
  }

  folder {
    basic_style()
    font_style()
  }

  file {
    basic_style()
    font_style()
  }

  hexagon {
    basic_style()
    font_style()
  }

  agent {
    basic_style()
    font_style()
  }

  artifact {
    basic_style()
    font_style()
  }

  card {
    basic_style()
    font_style()
  }

  stack {
    basic_style()
    font_style()
  }
}

'------------------------------------------------
' Common

skinparam {
  note {
    LIGHTCOLOR_style()
    font_style()
  }

  cloud {
    basic_style()
    font_style()
    arrow_style()
  }

  rectangle {
    basic_style()
    font_style()
  }

  storage {
    basic_style()
    font_style()
  }
}

' '------------------------------------------------
' ' WBS diagrams

' <style>
'   wbsDiagram {
'     arrow {
'       arrow_style()
'       lineColor $LIGHTCOLOR
'     }
'     node {
'       basic_style()
'       font_style()
'       lineColor $BORDERCOLOR
'     }
'   }
' </style>

' '------------------------------------------------
' ' Mindmap diagrams

' <style>
'   mindmapDiagram {
'     arrow {
'       arrow_style()
'       lineColor $LIGHTCOLOR
'     }
'     node {
'       basic_style()
'       font_style()
'       lineColor $BORDERCOLOR
'     }
'   }
' </style>



'==================================================================================================
' ADAPTIONS
'==================================================================================================

!ifndef NOADAPTATIONS
  ' Apply skin special adaptations

  !define ADAPTATIONS

  skinparam style strictuml

  '------------------------------------------------
  ' Class diagrams

  !startsub CLASS

    hide interface fields
    hide <<interface>> fields
    hide enum methods
    hide <<enumeration>> methods
    hide circle

    skinparam class {
      attributeIconSize 0
    }

  !endsub

  '------------------------------------------------
  ' State diagrams

  !startsub STATE

    hide empty description

  !endsub

  '------------------------------------------------
  ' Sequence diagrams

  !startsub SEQUENCE

    !ifdef TRANSPARENT
      skinparam sequence {
        !ifdef DARKSTYLE
          !$LIFELINEBG ?= "1b1b1b"
        !endif
        !ifdef LIGHTSTYLE
          !$LIFELINEBG ?= "aaa"
        !endif
        lifeLineBackgroundColor $LIFELINEBG
      }
    !endif

  !endsub
!endif
